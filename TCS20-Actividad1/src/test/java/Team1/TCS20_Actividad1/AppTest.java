package Team1.TCS20_Actividad1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import dto.Geometria;

public class AppTest 
extends TestCase
{
	@Test
	public void testAreaCuadrado() {
		double resultado = Geometria.areacuadrado(4);
		double esperado = 16;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaCirculo() {
		double resultado = Geometria.areaCirculo(2);
		double esperado = 12.57;
		assertEquals(esperado, resultado, 0.5);
	}
	
	@Test
	public void testAreaTriangulo() {
		double resultado = Geometria.areatriangulo(10,10);
		double esperado = 50;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaRectangulo() {
		double resultado = Geometria.arearectangulo(2,2);
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreapentagono() {
		double resultado = Geometria.areapentagono(4,4);
		double esperado = 8;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testArearombo() {
		double resultado = Geometria.arearombo(4,4);
		double esperado = 8;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testArearomboide() {
		double resultado = Geometria.arearomboide(2,2);
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreatrapecio() {
		double resultado = Geometria.areatrapecio(10,10,5);
		double esperado = 50;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura1() {
		String resultado = Geometria.figura(1);
		String esperado = "cuadrado";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura2() {
		String resultado = Geometria.figura(2);
		String esperado = "Circulo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura3() {
		String resultado = Geometria.figura(3);
		String esperado = "Triangulo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura4() {
		String resultado = Geometria.figura(4);
		String esperado = "Rectangulo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura5() {
		String resultado = Geometria.figura(5);
		String esperado = "Pentagono";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura6() {
		String resultado = Geometria.figura(6);
		String esperado = "Rombo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura7() {
		String resultado = Geometria.figura(7);
		String esperado = "Romboide";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura8() {
		String resultado = Geometria.figura(8);
		String esperado = "Trapecio";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFiguraDefault() {
		String resultado = Geometria.figura(0);
		String esperado = "Default";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testGetId() {
		Geometria g1 = new Geometria(5);
		assertEquals(5, g1.getId());
	}
	
	@Test
	public void testGetNom() {
		Geometria g1 = new Geometria();
		assertEquals("Default", g1.getNom());
	}
	
	@Test
	public void testGetArea() {
		Geometria g1 = new Geometria();
		assertEquals(1.0, g1.getArea());
	}
	
	@Test
	public void testSetId() {
		Geometria g1 = new Geometria();
		g1.setId(40);
		int resultado = g1.getId();
		int esperado = 40;
		assertEquals(esperado, resultado);
	}
	
	public void testSetNom() {
		Geometria g1 = new Geometria();
		g1.setNom("Prueba");
		String resultado = g1.getNom();
		String esperado = "Prueba";
		assertEquals(esperado, resultado);
	}
	
	public void testSetArea() {
		Geometria g1 = new Geometria();
		g1.setArea(2.0);
		double resultado = g1.getArea();
		double esperado = 2.0;
		assertEquals(esperado, resultado);
	}
	
	public void testToString() {
		Geometria g1 = new Geometria();
		String esperado = "Geometria [id=" + 9 + ", nom=" + "Default" + ", area=" + 1.0 + "]";
		String resultado = g1.toString();
		assertEquals(esperado, resultado);
	}
	
	
}

